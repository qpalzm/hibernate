package ru.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hibernate.model.Group;

public interface GroupRepository extends JpaRepository<Group, String> {

}
