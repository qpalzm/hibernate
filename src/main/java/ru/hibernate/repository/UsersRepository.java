package ru.hibernate.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.hibernate.model.User;

public interface UsersRepository extends JpaRepository<User, String> {

    @Query(value = "select * from user where location like :str%", nativeQuery = true)
    List<User> findWhereLocationStartsFrom(@Param("str") String str);

}
