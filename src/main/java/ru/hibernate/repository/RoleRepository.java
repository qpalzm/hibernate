package ru.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hibernate.model.Role;

public interface RoleRepository extends JpaRepository<Role, String> {

}
