package ru.hibernate.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_group")
public class Group {

    @Id
    private String id = UUID.randomUUID().toString();

    private Name name;

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", name=" + name +
                '}';
    }
}
