package ru.hibernate.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    private Name name;

    private String roleId;

    private String groupId;

    private String location;

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name=" + name +
                ", roleId='" + roleId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
