package ru.hibernate.model;

import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Name {

    private String name;

    private String description;

    public Name(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Name() {
    }

    @Override
    public String toString() {
        return "Name{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
