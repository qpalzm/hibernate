package ru.hibernate.process;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hibernate.model.Group;
import ru.hibernate.model.Name;
import ru.hibernate.model.Role;
import ru.hibernate.model.User;
import ru.hibernate.serivce.GroupService;
import ru.hibernate.serivce.RoleService;
import ru.hibernate.serivce.UsersService;

@Component
public class Process {

    @Autowired
    private UsersService usersService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private RoleService roleService;

    public void run(EntityManager em) {
        Role roleAdmin = new Role();
        roleAdmin.setName(new Name("admin", "role for admin"));
        roleAdmin.setLevel(0);

        roleService.createRole(em, roleAdmin);

        Role roleUser = new Role();
        roleUser.setName(new Name("user", "role for user"));
        roleUser.setLevel(99);

        roleService.createRole(em, roleUser);

        Group generalGroup = new Group();
        generalGroup.setName(new Name("general", "general group"));

        groupService.saveGroup(em, generalGroup);

        User admin = new User();
        admin.setRoleId(roleAdmin.getId());
        admin.setGroupId(generalGroup.getId());
        admin.setName(new Name("admin", "account for admin"));
        admin.setLocation("Moscow");

        usersService.createUser(em, admin);

        User user = new User();
        user.setGroupId(generalGroup.getId());
        user.setRoleId(roleUser.getId());
        user.setName(new Name("test", "account for test"));
        user.setLocation("Minsk");

        usersService.createUser(em, user);

        User userTest = new User();
        userTest.setGroupId(generalGroup.getId());
        userTest.setRoleId(roleUser.getId());
        userTest.setName(new Name("test", "account for test"));
        userTest.setLocation("Kiev");

        usersService.createUser(em, userTest);

        List<User> userList = usersService.findWhereLocationSStarts(em, "M");
        for (User u : userList) {
            System.out.println(u);
        }

        groupService.deleteGroup(em, generalGroup);

        Role testRole = new Role();
        testRole.setName(new Name("test", "role for test"));
        testRole.setLevel(50);

        Role role = roleService.createRole(em, testRole);
        System.out.println(role.toString());

        role.setLevel(45);
        Name name = role.getName();
        name.setName("update test");
        name.setDescription("role after update");
        role.setName(name);

        roleService.updateRole(em, role);

        List<Role> roleById = roleService.getRoleById(em, role.getId());
        for (Role r : roleById) {
            System.out.println(r);
        }
        roleService.deleteRole(em, role);
        List<Role> roleList = roleService.getAllRole(em);
        for (Role r : roleList) {
            System.out.println(r);
        }
    }

}
