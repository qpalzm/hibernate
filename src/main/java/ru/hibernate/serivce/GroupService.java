package ru.hibernate.serivce;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hibernate.model.Group;
import ru.hibernate.model.User;
import ru.hibernate.repository.GroupRepository;

@Service
public class GroupService {

    @Autowired
    private GroupRepository repository;

    @Autowired
    private UsersService usersService;

    public void saveGroup(EntityManager em, Group group) {
        em.getTransaction().begin();
        repository.save(group);
        em.getTransaction().commit();
    }

    public void deleteGroup(EntityManager em, Group group) {
        List<User> userByGroup = usersService.findUserByGroup(em, group.getId());
        if (userByGroup == null || !userByGroup.isEmpty()) {
            System.out.println("Delete or Update User for deleted Group");
            return;
        }
        em.getTransaction().begin();
        repository.delete(group);
        em.getTransaction().commit();
    }

}
