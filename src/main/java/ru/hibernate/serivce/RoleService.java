package ru.hibernate.serivce;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hibernate.model.Role;
import ru.hibernate.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    private RoleRepository repository;

    public Role createRole(EntityManager em, Role role) {
        em.getTransaction().begin();
        Role save = repository.save(role);
        em.getTransaction().commit();
        return save;
    }

    public List<Role> getRoleById(EntityManager em, String id) {
        em.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Role> query = criteriaBuilder.createQuery(Role.class);
        Root<Role> from = query.from(Role.class);
        query.select(from).where(criteriaBuilder.equal(from.get("id"), id));
        TypedQuery<Role> emQuery = em.createQuery(query);
        List<Role> resultList = emQuery.getResultList();
        em.getTransaction().commit();
        return resultList;
    }

    public void deleteRole(EntityManager em, Role role) {
        em.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete<Role> query = criteriaBuilder.createCriteriaDelete(Role.class);
        Root<Role> from = query.from(Role.class);
        query.where(criteriaBuilder.equal(from.get("id"), role.getId()));
        em.createQuery(query).executeUpdate();
        em.getTransaction().commit();
    }

    public void updateRole(EntityManager em, Role role) {
        em.getTransaction().begin();
        repository.save(role);
        em.getTransaction().commit();
    }

    public List<Role> getAllRole(EntityManager em) {
        em.getTransaction().begin();
        List<Role> all = repository.findAll();
        em.getTransaction().commit();
        return all;
    }
}
