package ru.hibernate.serivce;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hibernate.model.User;
import ru.hibernate.repository.UsersRepository;

@Service
public class UsersService {

    @Autowired
    private UsersRepository repository;

    public void createUser(EntityManager em, User user) {
        em.getTransaction().begin();
        repository.save(user);
        em.getTransaction().commit();
    }

    public void updateUser(EntityManager em, User user) {
        em.getTransaction().begin();
        repository.save(user);
        em.getTransaction().commit();
    }

    public List<User> findWhereLocationSStarts(EntityManager em, String str) {
        em.getTransaction().begin();
        List<User> whereNameStartsFrom = repository.findWhereLocationStartsFrom(str);
        em.getTransaction().commit();
        return whereNameStartsFrom;
    }

    public List<User> findUserByGroup(EntityManager em, String groupId) {
        em.getTransaction().begin();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> from = query.from(User.class);
        query.select(from).where(criteriaBuilder.equal(from.get("groupId"), groupId));
        TypedQuery<User> emQuery = em.createQuery(query);
        List<User> resultList = emQuery.getResultList();
        em.getTransaction().commit();
        return resultList;
    }
}
