package ru.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.hibernate.process.Process;

@EnableJpaRepositories
@EnableTransactionManagement
@Configuration
@ComponentScan
public class App {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(App.class);
        Process process = context.getBean(Process.class);
        EntityManagerFactory factory = context.getBean(EntityManagerFactory.class);
        EntityManager em = factory.createEntityManager();
        process.run(em);
        em.close();
        factory.close();

    }

}
